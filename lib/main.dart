import 'package:flutter/material.dart';
import 'pages/login.dart';

void main() {
  runApp(const MaterialApp(
    title: "Ventas a domicilio",
    home: LoginPage(),
  ));
}
